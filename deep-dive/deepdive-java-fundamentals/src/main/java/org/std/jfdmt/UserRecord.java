package org.std.jfdmt;

public record UserRecord(
        String userName,
        String transactionId
) {

}