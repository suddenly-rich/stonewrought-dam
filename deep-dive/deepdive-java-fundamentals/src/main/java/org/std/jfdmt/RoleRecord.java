package org.std.jfdmt;

public record RoleRecord(
        Long roleId,
        String roleName
) {
}
