package org.std.deepdive.mybatis.api.controller;

import org.springframework.web.bind.annotation.*;
import org.std.deepdive.mybatis.api.converter.UserConverter;
import org.std.deepdive.mybatis.mapper.UserMapper;
import org.std.deepdive.mybatis.model.UserModel;
import org.std.deepdive.mybatis.record.UserRecord;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserMapper userMapper;

    public UserController(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @PostMapping()
    @ResponseBody
    public UserRecord createUser(@RequestBody UserRecord userRecord) {
        UserModel userModel = UserConverter.INSTANCE.convertUserRecord(userRecord);
        userMapper.insert(userRecord);
        return userRecord;
    }
}
