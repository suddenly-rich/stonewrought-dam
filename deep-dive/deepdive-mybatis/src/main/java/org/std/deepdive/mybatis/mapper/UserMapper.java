package org.std.deepdive.mybatis.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper {

    void insert(Record userRecord);
}
