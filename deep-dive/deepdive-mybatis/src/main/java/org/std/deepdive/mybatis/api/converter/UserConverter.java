package org.std.deepdive.mybatis.api.converter;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.std.deepdive.mybatis.model.UserModel;
import org.std.deepdive.mybatis.model.UserResult;
import org.std.deepdive.mybatis.record.UserRecord;

@Mapper(
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValueIterableMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT
)
public interface UserConverter {

    UserConverter INSTANCE = Mappers.getMapper(UserConverter.class);

    UserModel convertUserRecord(UserRecord userRecord);

    UserRecord convertUserModel(UserModel userModel);

    UserModel convertUserResult(UserResult userResult);
}
