package org.std.caching.impl;

import lombok.extern.slf4j.Slf4j;
import org.std.caching.CommonResourceCache;

@Slf4j
public class AnimalResourceCache extends CommonResourceCache {

    @Override
    public Object getValue(String key) {
        return this.get(key, t -> {
            log.info("The cache Key is: "+ t + ". It's not exist");
            return t;
        });
    }
}
